#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from bangkk device
$(call inherit-product, device/motorola/bangkk/device.mk)

PRODUCT_DEVICE := bangkk
PRODUCT_NAME := lineage_bangkk
PRODUCT_BRAND := motorola
PRODUCT_MODEL := moto g84 5G
PRODUCT_MANUFACTURER := motorola

PRODUCT_GMS_CLIENTID_BASE := android-motorola

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="bangkk_g-user 14 U1TCS34M.22-64-19-4-2 9c98b release-keys"

BUILD_FINGERPRINT := motorola/bangkk_g/bangkk:14/U1TCS34M.22-64-19-4-2/9c98b:user/release-keys
